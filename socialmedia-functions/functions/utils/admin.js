const admin = require("firebase-admin");
const serviceAccount = require("../keys/serviceAccountKey.json");

admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: "https://socialmedia-a792a.firebaseio.com",
});

const db = admin.firestore();

module.exports = { admin, db };