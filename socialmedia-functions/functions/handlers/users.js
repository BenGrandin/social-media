const { db } = require('../utils/admin');


const signInValidator = require("../schemas/signIn");
const loginValidator = require("../schemas/login");
const firebase = require('firebase');
const config = require("../utils/config");
firebase.initializeApp(config);

let token, userId;

module.exports.signup = (req, res) => {
	const newUser = {
		...req.body,
	};

	const { error } = signInValidator.validate(newUser);

	if( error ) {

		if( error.details[0].context.key === "confirmPassword" ) {
			return res.status(400).json({ error: "confirmPassword isn't correct" });
		} else {
			return res.status(400).json({ error: error.details[0].message });
		}

	} else {
		db.doc(`/users/${newUser.handle}`).get()
			.then(doc => {
				if( doc.exists ) {
					return res.status(400).json({ handle: "this handle is already taken" });
				} else {
					return firebase.auth().createUserWithEmailAndPassword(newUser.email, newUser.password);
				}
			})
			.then(({ user }) => {
				userId = user.uid;
				return user.getIdToken();
			})
			.then(dataToken => {
				token = dataToken;
				const userCredentials = {
					handle: newUser.handle,
					email: newUser.email,
					createdAt: new Date().toISOString(),
					userId
				};
				return db.doc(`/users/${newUser.handle}`).set(userCredentials);
			})
			.then(() => {
				return res.status(201).json({ token });
			})
			.catch(err => {
				console.error(err);
				if( err.code === "auth/email-already-in-use" ) {
					return res.status(400).json({ email: "Email already in use" });
				} else {
					console.log(JSON.stringify(err));

					return res.status(500).json({ error: err.code + "toto" });
				}
			});
	}

};

module.exports.login = (req, res) => {
	const { email, password } = req.body;
	const user = {
		email,
		password
	};

	const { error } = loginValidator.validate(user);
	if( error ) {
		return res.status(400).json({ error: error.details[0].message });
	} else {
		firebase.auth().signInWithEmailAndPassword(user.email, user.password)
			.then(({ user }) => user.getIdToken())
			.then(token => res.json(token))
			.catch(err => {
				console.error(err);
				if( code === 'auth/wrong-password' ) {
					return res.status(403).json({ general: "Wrong credentials, please try again" });
				} else {
					return res.stats(500).json({ error: err.code });
				}
			});
	}

};
